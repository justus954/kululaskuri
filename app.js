var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var session = require('express-session');

var indexRouter = require('./routes/indexRoute');
var userRouter = require('./routes/userRoute');
var apiRouter = require('./routes/apiRoute');
var expenseRouter = require('./routes/expenseRoute');
var incomeRouter = require('./routes/incomeRoute');
var historyRouter = require('./routes/historyRoute');

var app = express();

// mongodb connection atlas
// mongodb old url parser is deprecated using new one
var mongoose = require('mongoose');
var mongoDB = 'mongodb+srv://IsoPahaSusi:HakkeritPoisHyi@kululaskuri-cluster-jz8wy.gcp.mongodb.net/kululaskuri';
mongoose.connect(mongoDB, { useNewUrlParser: true });
mongoose.Promise = global.Promise;
var db = mongoose.connection;
db.on('error', console.error.bind(console, 'MongoDB connection error:'));

// session handling
app.use(
    session({
        secret: 'ThisIsaSecretThatIsHardToCrackEverybodyWhoTriesareQuiteWack',
        resave: false,
        saveUninitialized: false,
        cookie: {
            httpOnly: true,
            secure: process.env.NODE_ENV === 'production',
            maxAge: 1000 * 60 * 60 * 24 * 7 // 7 days
        }
    }));

app.use(function (req, res, next) {
    if (req.session.userid == null || req.session.usermail == null) {
        req.session.userid = '';
        req.session.usermail = '';
    }
    next();
});

// global variable in all views
app.use(function (req, res, next) {
    app.locals.sessionuserid = req.session.userid;
    app.locals.sessionemail = req.session.usermail;
    next();
});

console.log('Session started');

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);
app.use('/user', userRouter);
app.use('/api', apiRouter);
app.use('/expense', expenseRouter);
app.use('/income', incomeRouter);
app.use('/history', historyRouter);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
    next(createError(404));
});

// error handler
app.use(function (err, req, res, next) {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};

    // render the error page
    res.status(err.status || 500);
    res.render('errorPage');
});

module.exports = app;
