var kayttajat = require('../models/userModel');
const { body, validationResult } = require('express-validator/check');
const { sanitizeBody } = require('express-validator/filter');
const bcrypt = require('bcrypt');
var async = require('async');

// Simple re-useable function for opening the page without login or register
function OpenWithNologin (req, res) {
    // check if there's already a login session, otherwise return normal index
    if (!req.session.userid || !req.session.usermail) {
        console.log('Page opened with no login');
        res.render('indexPage', { title: 'Express' });
    } else {
        console.log('Page opened with login');
        res.render('indexPage', { title: 'Express' });
    }
}

// basic index
exports.index = function (req, res) {
    OpenWithNologin(req, res);
};

// Logout where session gets nulled
exports.logout = function (req, res) {
    req.session.userid = '';
    req.session.usermail = '';
    res.redirect('/');
};

// Register page if user wants to register a new user
exports.register = function (req, res) {
    res.render('registerPage', { title: 'Express' });
};

// Register page if user wants to register a new user
exports.register_post = function (req, res, next) {
    body('RegisterUserEmail').isLength({ min: 1 }),
    body('RegisterUserPassword').isLength({ min: 1 }),
    sanitizeBody('RegisterUserEmail').trim().escape(),
    sanitizeBody('RegisterUserPassword').trim().escape(),
    async.parallel({
        kayttaja: function (callback) {
            kayttajat.find({ 'email': req.body.RegisterUserEmail }).exec(callback);
        }
    }, function (err, results) {
        if (err) { return next(err); }
        // Check that email is not in use
        if (results.kayttaja[0] != null) {
            OpenWithNologin(req, res);
        } else {
            // If email is brand new, create a new account
            if (req.body.RegisterUserPassword != null && req.body.RegisterUserEmail != null) {
                // Process request after validation and sanitization.

                // Extract the validation errors from a request.
                const errors = validationResult(req);

                // Create an user object with escaped and trimmed data.
                bcrypt.genSalt(15, function (err, salt) {
                    bcrypt.hash(req.body.RegisterUserPassword, salt, function (err, hash) {
                        if (err) { return next(err); } else {
                            var user = new kayttajat({
                                email: req.body.RegisterUserEmail,
                                password: hash
                            });
                            // Data from form is valid. Save user.
                            user.save(function (err) {
                                if (err) {
                                    // return next(err);
                                    res.render('registerPage', { title: 'Express' });
                                }
                                // successful - redirect to index with new user logged in
                                console.log('Registered an user.');
                                req.session.userid = user.id;
                                req.session.usermail = user.email;
                                req.session.sessionuserid = user.id;
                                req.session.sessionusermail = user.email;
                                res.render('indexPage', { title: 'Express', sessionemail: user.email, sessionuserid: user.id });
                            });
                        }
                    });
                });
            } else {
                // if email is in use and password is wrong
                OpenWithNologin(req, res);
            }
        }
    });
};

// Post on login
exports.login_post = function (req, res, next) {
    body('username').isLength({ min: 1 }),
    body('password').isLength({ min: 1 }),
    sanitizeBody('username').trim().escape(),
    sanitizeBody('password').trim().escape(),
    async.parallel({
        kayttaja: function (callback) {
            kayttajat.find({ 'email': req.body.username }).exec(callback);
        }
    }, function (err, results) {
        if (err) { return next(err); }
        if (results.kayttaja[0] != null) {
            bcrypt.compare(req.body.password, results.kayttaja[0].password, function (err, CompareSucceed) {
                if (err) {
                    return next(err);
                }
                if (CompareSucceed) {
                    console.log('Logged in');
                    req.session.userid = results.kayttaja[0].id;
                    req.session.usermail = results.kayttaja[0].email;
                    req.session.sessionuserid = results.kayttaja[0].id;
                    req.session.sessionusermail = results.kayttaja[0].email;
                    res.render('indexPage', { title: 'Express', sessionemail: results.kayttaja[0].email, sessionuserid: results.kayttaja[0].id });
                } else {
                    OpenWithNologin(req, res);
                }
            });
        } else {
            // If there is no account tied to this email, return to index
            OpenWithNologin(req, res);
        }
    });
};
