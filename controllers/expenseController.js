var ExpenseModel = require('../models/expenseModel');

// here are required validators
const { body, validationResult } = require('express-validator/check');
const { sanitizeBody } = require('express-validator/filter');

let pickerdatenow = new Date().toISOString().substring(0, 10);

// GET all expenses
exports.expenseIndex = function userList (req, res, next) {
    if (req.session.userid) {
        ExpenseModel.find({ userid: req.session.userid }, '-__v').exec(function execExpenses (err, listExpenses) {
            if (err) { return next(err); }
            res.render('expensePage', { listExpenses: listExpenses });
        });
    } else {
        res.redirect('/expense/create');
    }
};

// GET expense form
exports.expenseCreateGet = function expenseGetCreate (req, res, next) {
    const errors = validationResult(req);

    res.render('expenseFormPage', { title: 'Create Expense', expense: req.body, paramsurl: req.url, errors: errors.array(), pickerdatenow: pickerdatenow });
};

// POST expense create form
exports.expenseCreatePost = [
    // Validate fields.
    body('fullmonth').isISO8601().trim().withMessage('Entered date has to be date in the form of "YYYY-MM-DD". '),
    body('rent').isNumeric().trim().withMessage('Rent has to be a number.'),
    body('food').isNumeric().trim().withMessage('Food has to be a number.'),
    body('drink').isNumeric().trim().withMessage('Drink has to be a number.'),
    body('other').isNumeric().trim().withMessage('Other has to be a number.'),

    // Sanitize fields.
    sanitizeBody('fullmonth').trim().escape(),
    sanitizeBody('rent').trim().escape(),
    sanitizeBody('food').trim().escape(),
    sanitizeBody('drink').trim().escape(),
    sanitizeBody('other').trim().escape(),

    // Process request after validation and sanitization.
    (req, res, next) => {
        // Extract the validation errors from a request.
        const errors = validationResult(req);

        if (!errors.isEmpty()) {
            // There are errors. Render form again with sanitized values/errors messages.
            res.render('expenseFormPage', { title: 'Create Expense', expense: req.body, errors: errors.array(), paramsurl: req.url, pickerdatenow: pickerdatenow });
        } else {
            // Data from form is valid.

            // Create an expense object with escaped and trimmed data.
            if (req.session.userid) {
                var expense = new ExpenseModel(
                    {
                        userid: req.session.userid,
                        date: req.body.fullmonth,
                        rent: req.body.rent,
                        food: req.body.food,
                        drink: req.body.drink,
                        other: req.body.other
                    });
                expense.save(function errexpensepost (err) {
                    if (err) { return next(err); }
                    // Successful - redirect to new expense record.
                    res.redirect(expense.url);
                });
            } else {
                res.render('expenseFormPage', { title: 'Create Expense', expense: req.body, errors: errors.array(), paramsurl: req.url, pickerdatenow: pickerdatenow });
            }
        }
    }
];

// Display detail page for a specific Expense.
exports.expenseDetail = function expenseDetail (req, res, next) {
    ExpenseModel.findById(req.params.id)
        .exec(function execExpense (err, listExpense) {
            if (err) { return next(err); }
            // Successful, so render
            res.render('expenseDetailPage', { title: 'Expense information', expense: listExpense });
        });
};

exports.expenseUpdateGet = function expenseupdGet (req, res, next) {
    const errors = validationResult(req);

    ExpenseModel.findById(req.params.id)
        .exec(function execExpenseUpd (err, listExpense) {
            if (err) { return next(err); }
            if (listExpense === null) { res.redirect('/expense/'); }
            // Successful, so render
            res.render('expenseFormPage', { title: 'Update expense', expense: listExpense, paramsurl: req.url, errors: errors.array(), pickerdatenow: pickerdatenow });
        });
};

exports.expenseUpdatePost = [
    // Validate fields.
    body('fullmonth').isISO8601().trim().withMessage('Entered date has to be date in the form of "YYYY-MM-DD". '),
    body('rent').isNumeric().trim().withMessage('Rent has to be a number.'),
    body('food').isNumeric().trim().withMessage('Food has to be a number.'),
    body('drink').isNumeric().trim().withMessage('Drink has to be a number.'),
    body('other').isNumeric().trim().withMessage('Other has to be a number.'),

    // Sanitize fields.
    sanitizeBody('fullmonth').trim().escape(),
    sanitizeBody('rent').trim().escape(),
    sanitizeBody('food').trim().escape(),
    sanitizeBody('drink').trim().escape(),
    sanitizeBody('other').trim().escape(),

    // Process request after validation and sanitization.
    (req, res, next) => {
        // Extract the validation errors from a request.
        const errors = validationResult(req);

        if (!errors.isEmpty()) {
            // There are errors. Render form again with sanitized values/errors messages.
            res.render('expenseFormPage', { title: 'Update expense', expense: req.body, errors: errors.array(), paramsurl: req.url, pickerdatenow: pickerdatenow });
        } else {
            // Data from form is valid.

            // Create an expense object with escaped and trimmed data.
            var expense = new ExpenseModel(
                {
                    userid: req.session.userid,
                    date: req.body.fullmonth,
                    rent: req.body.rent,
                    food: req.body.food,
                    drink: req.body.drink,
                    other: req.body.other,
                    _id: req.params.id
                });
            ExpenseModel.findByIdAndUpdate(req.params.id, expense, {}, function dbupdexpense (err, theExpense) {
                if (err) { return next(err); }
                // Successful - redirect to expense detail page.
                res.redirect(theExpense.url);
            });
        }
    }
];

exports.expenseDeleteGet = function expensedelGet (req, res, next) {
    ExpenseModel.findById(req.params.id)
        .exec(function execExpenseDel (err, listExpense) {
            if (err) { return next(err); }
            if (listExpense === null) { res.redirect('/expense/'); }
            // Successful, so render
            res.render('expenseDeletePage', { title: 'Delete expense', expense: listExpense });
        });
};

exports.expenseDeletePost = function expensedelPost (req, res, next) {
    ExpenseModel.findByIdAndRemove(req.body.expenseid, function deleteExpense (err) {
        if (err) { return next(err); }
        // Success - go to expense list
        res.redirect('/expense/');
    });
};
