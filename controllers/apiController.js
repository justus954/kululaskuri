var UserModel = require('../models/userModel');
var ExpenseModel = require('../models/expenseModel');
var incomeModel = require('../models/incomeModel');

exports.apiIndex = function apiList (req, res, next) {
    UserModel.find({}, '-__v -password')
        .exec(function execUser (err, listUsers) {
            if (err) { return next(err); }
            // Successful, so render
            res.send(listUsers);
        });
};

exports.apiExpense = function expenseList (req, res, next) {
    ExpenseModel.find({}, '-__v')
        .exec(function execExpense (err, listExpenses) {
            if (err) { return next(err); }
            // Successful, so render
            res.send(listExpenses);
        });
};

exports.apiIncome = function incomeList (req, res, next) {
    incomeModel.find({}, '-__v')
        .exec(function execIncome (err, listIncomes) {
            if (err) { return next(err); }
            // Successful, so render
            res.send(listIncomes);
        });
};
