var incomeModel = require('../models/incomeModel');

// here are required validators
const { body, validationResult } = require('express-validator/check');
const { sanitizeBody } = require('express-validator/filter');

let pickerdatenow = new Date().toISOString().substring(0, 10);

// GET all income
exports.incomeIndex = function userList (req, res, next) {
   if (req.session.userid) {
        incomeModel.find({ userid: req.session.userid }, '-__v').exec(function execUser (err, listIncomes) {
        if (err) { return next(err); }
        res.render('incomePage', { listincomes: listIncomes });
        });
    } else {
        res.redirect('/income/create');
    }
};

// GET income form
exports.incomeCreateGet = function incomeGetCreate (req, res, next) {
    const errors = validationResult(req);

    res.render('incomeFormPage', { title: 'Create income', income: req.body, paramsurl: req.url, errors: errors.array(), pickerdatenow: pickerdatenow });
};

// POST income create form
exports.incomeCreatePost = [
    // Validate fields.
    body('fullmonth').isISO8601().trim().withMessage('Entered date has to be date in the form of "YYYY-MM-DD". '),
    body('studentAllowance').isNumeric().trim().withMessage('Student allowance has to be a number.'),
    body('salary').isNumeric().trim().withMessage('Salary has to be a number.'),
    body('studentLoan').isNumeric().trim().withMessage('Student loan has to be a number.'),
    body('otherIncome').isNumeric().trim().withMessage('Other income has to be a number.'),

    // Sanitize fields.
    sanitizeBody('fullmonth').trim().escape(),
    sanitizeBody('studentAllowance').trim().escape(),
    sanitizeBody('salary').trim().escape(),
    sanitizeBody('studentLoan').trim().escape(),
    sanitizeBody('otherIncome').trim().escape(),

    // Process request after validation and sanitization.
    (req, res, next) => {
        // Extract the validation errors from a request.
        const errors = validationResult(req);

        if (!errors.isEmpty()) {
            // There are errors. Render form again with sanitized values/errors messages.
            res.render('incomeFormPage', { title: 'Create income', income: req.body, errors: errors.array(), paramsurl: req.url, pickerdatenow: pickerdatenow });
        } else {
            // Data from form is valid.

            // Create an income object with escaped and trimmed data.
            if (req.session.userid) {
                var income = new incomeModel(
                    {
                        userid: req.session.userid,
                        date: req.body.fullmonth,
                        studentAllowance: req.body.studentAllowance,
                        salary: req.body.salary,
                        studentLoan: req.body.studentLoan,
                        otherIncome: req.body.otherIncome
                    });
                income.save(function errincomepost (err) {
                    if (err) { return next(err); }
                    // Successful - redirect to new income record.
                    res.redirect(income.url);
                });
            } else {
                res.render('incomeFormPage', { title: 'Create Income', income: req.body, errors: errors.array(), paramsurl: req.url, pickerdatenow: pickerdatenow });
            }
        }
    }
];

// Display detail page for a specific income.
exports.incomeDetail = function incomeDetail (req, res, next) {
    incomeModel.findById(req.params.id)
        .exec(function execincome (err, listincome) {
            if (err) { return next(err); }
            // Successful, so render
            res.render('incomeDetailPage', { title: 'income information', income: listincome });
        });
};

exports.incomeUpdateGet = function incomeupdGet (req, res, next) {
    const errors = validationResult(req);

    incomeModel.findById(req.params.id)
        .exec(function execincomeUpd (err, listincome) {
            if (err) { return next(err); }
            if (listincome === null) { res.redirect('/income/'); }
            // Successful, so render
            res.render('incomeFormPage', { title: 'Update income', income: listincome, paramsurl: req.url, errors: errors.array(), pickerdatenow: pickerdatenow });
        });
};

exports.incomeUpdatePost = [
    // Validate fields.
    body('fullmonth').isISO8601().trim().withMessage('Entered date has to be date in the form of "YYYY-MM-DD". '),
    body('studentAllowance').isNumeric().trim().withMessage('Student allowance has to be a number.'),
    body('salary').isNumeric().trim().withMessage('Salary has to be a number.'),
    body('studentLoan').isNumeric().trim().withMessage('Student loan has to be a number.'),
    body('otherIncome').isNumeric().trim().withMessage('Other income has to be a number.'),

    // Sanitize fields.
    sanitizeBody('fullmonth').trim().escape(),
    sanitizeBody('studentAllowance').trim().escape(),
    sanitizeBody('salary').trim().escape(),
    sanitizeBody('studentLoan').trim().escape(),
    sanitizeBody('otherIncome').trim().escape(),

    // Process request after validation and sanitization.
    (req, res, next) => {
        // Extract the validation errors from a request.
        const errors = validationResult(req);

        if (!errors.isEmpty()) {
            // There are errors. Render form again with sanitized values/errors messages.
            res.render('incomeFormPage', { title: 'Update income', income: req.body, errors: errors.array(), paramsurl: req.url, pickerdatenow: pickerdatenow });
        } else {
            // Data from form is valid.

            // Create an income object with escaped and trimmed data.
            var income = new incomeModel(
                {
                    userid: req.session.userid,
                    date: req.body.fullmonth,
                    studentAllowance: req.body.studentAllowance,
                    salary: req.body.salary,
                    studentLoan: req.body.studentLoan,
                    otherIncome: req.body.otherIncome,
                    _id: req.params.id
                });
            incomeModel.findByIdAndUpdate(req.params.id, income, {}, function dbupdincome (err, theincome) {
                if (err) { return next(err); }
                // Successful - redirect to income detail page.
                res.redirect(theincome.url);
            });
        }
    }
];

exports.incomeDeleteGet = function incomedelGet (req, res, next) {
    incomeModel.findById(req.params.id)
        .exec(function execincomeDel (err, listincome) {
            if (err) { return next(err); }
            if (listincome === null) { res.redirect('/income/'); }
            // Successful, so render
            res.render('incomeDeletePage', { title: 'Delete income', income: listincome });
        });
};

exports.incomeDeletePost = function incomedelPost (req, res, next) {
    incomeModel.findByIdAndRemove(req.body.incomeid, function deleteincome (err) {
        if (err) { return next(err); }
        // Success - go to income list
        res.redirect('/income/');
    });
};
