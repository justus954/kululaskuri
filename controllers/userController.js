var users = require('../models/userModel');
const bcrypt = require('bcrypt');
const { body, validationResult } = require('express-validator/check');
const { sanitizeBody } = require('express-validator/filter');

exports.userIndex = function userList (req, res, next) {
    users.find({}, '-__v').exec(function execUser (err, listUsers) {
        if (err) { return next(err); }
        res.render('userPage', { listUsers: listUsers });
    });
};

exports.userIndex2 = function userList (req, res, next) {
    users.find({}, '-__v').exec(function execUser (err, listUsers) {
        if (err) { return next(err); }
        res.render('passwordPage');
    });
};

exports.userIndex3 = function passw (req, res, next) {
    var email = req.body.email;
    var password = req.body.new;
    var confirmation = req.body.confirm;

    users.find({}, '-_id email').exec(function execUser (err, listUsers) {
        if (err) { return next(err); }
        if (password === confirmation & JSON.stringify(listUsers).includes(email)) {
            bcrypt.genSalt(15, function (err, salt) {
                if (err) { return next(err); }
                bcrypt.hash(req.body.new, salt, function (err, hash) {
                    if (err) { return next(err); }
                    users.findOneAndUpdate({ email: req.body.email }, { $set: { email: req.body.email, password: hash } }, { new: true }, function changepassword (err, clear) {
                        if (err) { return next(err); }
                        res.render('submitPage');
                    });
                });
            });
        } else {
            res.render('rejectPage');
        }
    });
};
