var ExpenseModel = require('../models/expenseModel');
var IncomeModel = require('../models/incomeModel');

// GET all expenses and incomes
exports.historyIndex = function userList (req, res, next) {
    if (req.session.userid) {
        ExpenseModel.find({ userid: req.session.userid }, '-__v').exec(function execExpenses (err, listExpenses) {
            if (err) { return next(err); }

            IncomeModel.find({ userid: req.session.userid }, '-__v').exec(function execIncomes (err, listIncomes) {
                if (err) { return next(err); }
                res.render('historyPage', { expenses: listExpenses, incomes: listIncomes });
            });
        });
    } else {
        res.redirect('/');
    }
};
