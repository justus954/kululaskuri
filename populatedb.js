var Usermodel = require('./models/usermodel');
var Expensemodel = require('./models/expensemodel');
var Incomemodel = require('./models/expensemodel');

// connection to db
var mongoose = require('mongoose');
var mongoDB = 'mongodb+srv://IsoPahaSusi:HakkeritPoisHyi@kululaskuri-cluster-jz8wy.gcp.mongodb.net/kululaskuri';
mongoose.connect(mongoDB, { useNewUrlParser: true });
mongoose.Promise = global.Promise;
var db = mongoose.connection;
db.on('error', console.error.bind(console, 'MongoDB connection error:'));

// insert new data to usermodel
var user1 = new Usermodel({ email: 'anatesti@testi.com', password: 'uli' });
var expense1 = new Expensemodel({ rent: 30, food: 60, drink: 70, other: 90 });
var income1 = new Incomemodel({ studentAllowance: 20, salary: 30, studentLoan: 50, otherIncome: 0});

// save the info to db

user1.save(function (err, user) {
    if (err) return console.error(err);
    console.log(user.email + ' saved to user collection.');
});

// save the info to db
expense1.save(function (err, expense) {
    if (err) return console.error(err);
    console.log(expense.rent + ' saved to expense collection.');
});

// save the info to db
income1.save(function (err, income) {
    if (err) return console.error(err);
    console.log(income.studentAllowance + ' saved to income collection.');
});
