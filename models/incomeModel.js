var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var incomeSchema = new Schema(
    {
        userid: { type: Schema.Types.ObjectId, ref: 'User' },
        date: { type: Date, default: Date.now },
        studentAllowance: { type: Number, default: 0 },
        salary: { type: Number, default: 0 },
        studentLoan: { type: Number, default: 0 },
        otherIncome: { type: Number, default: 0 }
    }
);

// Virtual for Income's URL
incomeSchema
    .virtual('url')
    .get(function virtualIncomeUrl () {
        return '/income/' + this._id;
    });

incomeSchema
    .virtual('fullmonth')
    .get(function fullMonth () {
        return this.date.toLocaleDateString('en-US', { month: 'long' });
    });

incomeSchema
    .virtual('listdate')
    .get(function listDate () {
        return this.date.toDateString();
    });

incomeSchema
    .virtual('pickerdate')
    .get(function pickerDate () {
        return this.date.toISOString().substring(0, 10);
    });

// Export model
module.exports = mongoose.model('Income', incomeSchema);
