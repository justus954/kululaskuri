var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var UserSchema = new Schema(
    {
        email: { type: String, required: true, unique: true, max: 100 },
        password: { type: String, required: true, max: 100 }
    }
);

// Virtual for User's URL
UserSchema
    .virtual('url')
    .get(function virtualUserUrl () {
        return 'user/' + this._id;
    });

// Export model
module.exports = mongoose.model('User', UserSchema);
