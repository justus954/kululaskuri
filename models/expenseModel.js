var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var ExpenseSchema = new Schema(
    {
        userid: { type: Schema.Types.ObjectId, ref: 'User' },
        date: { type: Date, default: Date.now },
        rent: { type: Number, default: 0 },
        food: { type: Number, default: 0 },
        drink: { type: Number, default: 0 },
        other: { type: Number, default: 0 }
    }
);

// Virtual for Expense's URL
ExpenseSchema
    .virtual('url')
    .get(function virtualExpenseUrl () {
        return '/expense/' + this._id;
    });

ExpenseSchema
    .virtual('fullmonth')
    .get(function fullMonth () {
        return this.date.toLocaleDateString('en-US', { month: 'long' });
    });

ExpenseSchema
    .virtual('listdate')
    .get(function listDate () {
        return this.date.toDateString();
    });

ExpenseSchema
    .virtual('pickerdate')
    .get(function pickerDate () {
        return this.date.toISOString().substring(0, 10);
    });

// Export model
module.exports = mongoose.model('Expense', ExpenseSchema);
