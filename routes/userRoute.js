var express = require('express');
var router = express.Router();
var userrouter = require('../controllers/userController');
var users = require('../models/userModel');

router.get('/', userrouter.userIndex);
router.get('/change', userrouter.userIndex2);
router.post('/submitted', userrouter.userIndex3);

module.exports = router;
