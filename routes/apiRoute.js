var express = require('express');
var router = express.Router();
var apiController = require('../controllers/apiController');

// API GET users
router.get('/', apiController.apiIndex);
router.get('/expense', apiController.apiExpense);
router.get('/income', apiController.apiIncome);

module.exports = router;
