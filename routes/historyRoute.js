var express = require('express');
var router = express.Router();
var historyController = require('../controllers/historyController');

// GET history index
router.get('/', historyController.historyIndex);

module.exports = router;
