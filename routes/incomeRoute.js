var express = require('express');
var router = express.Router();
var incomeController = require('../controllers/incomeController');

// GET income index
router.get('/', incomeController.incomeIndex);

// GET income form
router.get('/create', incomeController.incomeCreateGet);

// POST income form
router.post('/create', incomeController.incomeCreatePost);

// GET one income
router.get('/:id', incomeController.incomeDetail);

// GET one income to update
router.get('/:id/update', incomeController.incomeUpdateGet);

// POST one income to update
router.post('/:id/update', incomeController.incomeUpdatePost);

// GET one income to delete
router.get('/:id/delete', incomeController.incomeDeleteGet);

// POST one income to delete
router.post('/:id/delete', incomeController.incomeDeletePost);

module.exports = router;


