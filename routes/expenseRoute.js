var express = require('express');
var router = express.Router();
var expenseController = require('../controllers/expenseController');

// GET expense index
router.get('/', expenseController.expenseIndex);

// GET expense form
router.get('/create', expenseController.expenseCreateGet);

// POST expense form
router.post('/create', expenseController.expenseCreatePost);

// GET one expense
router.get('/:id', expenseController.expenseDetail);

// GET one expense to update
router.get('/:id/update', expenseController.expenseUpdateGet);

// POST one expense to update
router.post('/:id/update', expenseController.expenseUpdatePost);

// GET one expense to delete
router.get('/:id/delete', expenseController.expenseDeleteGet);

// POST one expense to delete
router.post('/:id/delete', expenseController.expenseDeletePost);

module.exports = router;
