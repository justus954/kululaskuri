var express = require('express');
var router = express.Router();

//control modules
var index_controller = require('../controllers/indexController');


// GET catalog home page.
router.get('/', index_controller.index);

// POST request for login/register.
router.post('/', index_controller.login_post);

// GET request for logout.
router.get('/logout', index_controller.logout);

// GET request for register.
router.get('/register', index_controller.register);

// POST request for register.
router.post('/register', index_controller.register_post);

module.exports = router;
